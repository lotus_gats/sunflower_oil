'use strict';

import clientsSlides from './clientsSlides';
import Slider from './slider';



const clientSlider = new Slider({
    slider: document.querySelector('.clients_content'),
    data: clientsSlides
});

const addParallax = () => {
    const sections = [
        document.querySelector('.general'),
        document.querySelector('.delivery')
    ];

    if (navigator.platform !== 'iPhone'
        && sections.some(section => getComputedStyle(section).backgroundAttachment !== 'fixed')) {
        sections.forEach(section => {
            section.style.backgroundAttachment = 'fixed';
        })
    }
};

const scrollingAnimate = () => {
    const windowHeight = window.innerHeight;
    const elements = [document.querySelector('.service__content')];
    const info = {
        service_content: {
            element: elements[0],
            className: 'service__content',
            offset: elements[0].getBoundingClientRect().y,
            height: parseInt(getComputedStyle(elements[0]).height)
        }
    };

    for (let key in info) {

        if (info.hasOwnProperty(key)) {

            if (info[key].element.classList.contains('hidden') &&
                info[key].offset + info[key].height / 2 < windowHeight) {
                document.querySelector(`.${info[key].className}`).classList.remove('hidden')
            }
        }
    }
};
document.addEventListener('DOMContentLoaded', () => {
    clientSlider.initialize();
    addParallax();
});

document.addEventListener('scroll', () => {
    scrollingAnimate();
});


document.addEventListener('click', (e) => {
    const clickedElement = e.target;
    const dots = document.querySelectorAll('.clients_dots a');
    const modal = document.querySelector('.modal--window');

    // slider of clients
    if (clickedElement.parentNode.classList.contains('clients_nav')) {

        if (clickedElement.classList.contains('slider_nav_left')) {
            clientSlider.action({
                side: 'left'
            });
            clientSlider.refresh();
        }
        else if (clickedElement.classList.contains('slider_nav_right')) {
            clientSlider.action({
                side: 'right'
            });
            clientSlider.refresh();
        }
    }

    // dots clicking
    else if (clickedElement.parentNode.classList.contains('clients_dots')) {
        e.preventDefault();

        if (!clickedElement.classList.contains('active')) {

            for (let i = 0; i < dots.length; i++) {

                if (dots[i].classList.contains('active')) {
                    dots[i].classList.remove('active');
                }
                else if (dots[i] === clickedElement) {
                    clientSlider.action({
                        number: i
                    });
                    clientSlider.refresh();
                    setTimeout(() => {
                        dots[i].classList.add('active');
                    }, 500)
                }
            }
        }
    }

    else if (clickedElement.tagName === 'BUTTON') {

        if (modal.classList.contains('modal--hidden')) {
            modal.classList.remove('modal--hidden');
        }
    }

    else if (clickedElement.classList.contains('modal--window__close')
        || clickedElement.classList.contains('modal--window__overlay')) {
        modal.classList.add('modal--hidden');
    }

    else if (clickedElement.classList.contains('submit')) {
        const modalSwitching = (text, isError) => {
            const responsiveModal = document.querySelector('.responsive--modal');

            if (responsiveModal.classList.contains('modal--hidden')) {
                modal.classList.add('modal--hidden');
                responsiveModal.querySelector('.responsive--text').textContent = text;
                responsiveModal.classList.remove('modal--hidden');
                setTimeout(() => {
                    responsiveModal.classList.add('modal--hidden');
                    isError ? modal.classList.remove('modal--hidden') : modal.querySelector('form').reset();
                }, 1500)
            }
        };
        e.preventDefault();
        const user_id = 302632059;
        const fieldsIDs = ['userName', 'userPhone'];
        const values = fieldsIDs.map(id => document.querySelector(`#${id}`).value);
        telegramRequest(user_id, values).then(() => {
            modalSwitching('Заявка успешно отправлена', false)
        }).catch(err => {
            switch (err) {
                case 'Some fields are empty':
                    modalSwitching('Заполните все поля', true);
                    break;
                case 'Bad request':
                    modalSwitching('Повторите попытку', true);
                    break;
                default:
                    break;
            }
        })
    }
});

const telegramRequest = (id, values) => new Promise((resolve, reject) => {
    const space = '%0A';
    const isFull = () => new Promise((resolve, reject) =>
        resolve(values.reduce((string, value) => value ?
            string + space + value : reject(`Some fields are empty`), '')));
    isFull().then(text => {
        fetch(`https://api.telegram.org/bot383655115:AAFbKchTA9zDxOSfRW-8IVH2A6r3VTAjUV4/sendMessage?chat_id=${id}&text=${text}`)
            .then((res) => res.status === 200 ? resolve(res) : reject('Bad request'))
    }).catch(err => {
        reject(err);
    })
});
