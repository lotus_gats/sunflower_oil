module.exports = [
    {
        name: `«McDonald's»`,
        icon: `mcdonalds.png`
    },
    {
        name: '«Pomodoro»',
        icon: 'pomodoro.png'
    },
    {
        name: '«StarPizza»',
        icon: 'starpizza.png'
    },
    {
        name: '«Сушия»',
        icon: 'sushiya.png'
    },
    {
        name: '«SmileFood»',
        icon: 'smilefood.jpg'
    },
    {
        name: `«Mario's»`,
        icon: 'mario.png'
    }
];